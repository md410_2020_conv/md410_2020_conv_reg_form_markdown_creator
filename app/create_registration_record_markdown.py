""" Create a registration record for the Lions MD410 2020 Convenrtion
from form data from the convention website
"""

import attr
import json
import os.path

from md410_2020_conv_common.db import DB as common_db

REG_TYPES = {
    "full": ("Full", 1285),
    "banquet": ("Banquet", 500),
    "convention": ("MD410 Convention", 400),
    "theme": ("Theme Evening", 450),
}

EXTRA_TYPES = {"pins": ("Convention Pin", 55)}

ORDERS = ("First", "Second")

TABLES = {
    "registree": ("md410_2020_conv", "registree"),
    "club": ("md410_2020_conv", "club"),
    "partner_program": ("md410_2020_conv", "partner_program"),
    "full_reg": ("md410_2020_conv", "full_reg"),
    "partial_reg": ("md410_2020_conv", "partial_reg"),
    "pins": ("md410_2020_conv", "pins"),
    "registree_pair": ("md410_2020_conv", "registree_pair"),
}


@attr.s
class Registree(object):
    reg_num = attr.ib()
    timestamp = attr.ib()
    first_names = attr.ib()
    last_name = attr.ib()
    cell = attr.ib()
    email = attr.ib()
    dietary = attr.ib()
    disability = attr.ib()
    name_badge = attr.ib()
    first_mdc = attr.ib()
    mjf_lunch = attr.ib()
    pdg_breakfast = attr.ib()
    sharks_board = attr.ib()
    golf = attr.ib()
    sight_seeing = attr.ib()
    service_project = attr.ib()
    
    def __attrs_post_init__(self):
        self.auto_name_badge = False
        if not self.name_badge:
            self.name_badge = f"{self.first_names} {self.last_name}"
            self.auto_name_badge = True

    def render(self):
        out = []
        out.append(f"* **Registration Number**: MDC{self.reg_num:03}")
        out.append(f"* **First Name(s):** {self.first_names}")
        out.append(f"* **Last Name:** {self.last_name}")
        if hasattr(self, "cell"):
            out.append(f"* **Cell Phone:** {self.cell}")
        if hasattr(self, "email"):
            out.append(f"* **Email Address:** {self.email}")
        if hasattr(self, "club"):
            out.append(f"* **Club:** {self.club}")
        if hasattr(self, "dietary"):
            out.append(
                f"* **Dietary Requirements:** {self.dietary if self.dietary else 'None'}"
            )
        if hasattr(self, "disability"):
            out.append(
                f"* **Disabilities:** {self.disability if self.disability else 'None'}"
            )
        if hasattr(self, "partner_program"):
            out.append(
                f"* **Interested in a Partner's Program:** {'Yes' if self.partner_program else 'No'}"
            )
        if hasattr(self, "first_mdc"):
            out.append(
                f"* **This will be the attendee's first MD Convention:** {'Yes' if self.first_mdc else 'No'}"
            )
        if hasattr(self, "mjf_lunch"):
            out.append(
                f"* **Attendee will attend the Melvin Jones lunch:** {'Yes' if self.mjf_lunch else 'No'}"
            )
        if (self, "pdg_breakfast"):
            out.append(
                f"* **Attendee will attend the PDG's breakfast:** {'Yes' if self.pdg_breakfast else 'No'}"
            )
        if (self, "sharks_board"):
            out.append(
                f"* **Attendee is interested in a tour of the KwaZulu-Natal Sharks Board on Thursday 31 April (at an additional cost and offered subject to demand):** {'Yes' if self.sharks_board else 'No'}"
            )
        if (self, "golf"):
            out.append(
                f"* **Attendee is interested in a round of golf on Friday 1 May (at an additional cost and offered subject to demand):** {'Yes' if self.golf else 'No'}"
            )
        if (self, "sight_seeing"):
            out.append(
                f"* **Attendee is interested in a sight-seeing tour of Durban on Friday 1 May (at an additional cost and offered subject to demand):** {'Yes' if self.sight_seeing else 'No'}"
            )
        if (self, "service_project"):
            out.append(
                f"* **Attendee is interested in a service project in Durban on Friday 1 May (possibly at an additional cost and offered subject to demand):** {'Yes' if self.service_project else 'No'}"
            )
        if hasattr(self, "name_badge"):
            out.append(f"* **Details On Name Badge:** {self.name_badge}")
        if self.auto_name_badge:
            out.append("")
            out.append(
                f"**The name badge details were generated from the first and last names because no name badge details were supplied on the registration form. Please contact the registration team if you would like to update these details.**"
            )
        return out


@attr.s
class LionRegistree(Registree):
    club = attr.ib()

    def __attrs_post_init__(self):
        self.lion = True
        super().__attrs_post_init__()


@attr.s
class NonLionRegistree(Registree):
    partner_program = attr.ib(default=False)

    def __attrs_post_init__(self):
        self.lion = False
        super().__attrs_post_init__()


@attr.s
class Registration(object):
    full = attr.ib()
    banquet = attr.ib()
    convention = attr.ib()
    theme = attr.ib()

    def __attrs_post_init__(self):
        self.attrs = []

    def render(self):
        out = []
        self.cost = 0
        for attr in ("full", "banquet", "convention", "theme"):
            a = getattr(self, attr)
            if a:
                (description, cost) = REG_TYPES[attr]
                self.cost += cost * a
                out.append(
                    f"* **{a} {description} Registration{'s' if a > 1 else ''}:** R{cost * a}"
                )
        return out


@attr.s
class Extras(object):
    pins = attr.ib()

    def __attrs_post_init__(self):
        self.attrs = ("pins",)

    def __bool__(self):
        return bool(sum([getattr(self, attr) for attr in self.attrs]))

    def render(self):
        out = []
        self.cost = 0
        for attr in self.attrs:
            a = getattr(self, attr)
            if a:
                (description, cost) = EXTRA_TYPES[attr]
                self.cost += cost * a
                out.append(
                    f"* **{a} {description}{'s' if a > 1 else ''}:** R{cost * a}"
                )
        return out


@attr.s
class RegistrationRecord(object):
    attendees = attr.ib()
    registration = attr.ib()
    extras = attr.ib(default=False)
    out_dir = attr.ib(default=None)

    def __make_names(self, attr):
        self.names.append(
            f"{attr.first_names[0].lower()}_{attr.last_name.replace(' ','_').lower()}"
        )

    def __payment_details(self):
        reg_nums = "/".join(f"{r:03}" for r in self.reg_nums)
        self.out.append(f"# Payment Details {{-}}")
        self.out.append(
            f"""\

Please make all payments to this account:

* **Bank**: Nedbank
* **Account Type**: Savings Account
* **Branch Code**: 138026
* **Account Number**: 2015836799
* **Account Name**: Convention 2020

Please make EFT payments rather than cash deposits wherever possible.

Use the reference "*MDC{reg_nums}*" when making payments. 

Your registration will be finalised on the payment of a deposit of R{300 * len(self.reg_nums)}{' (R300 per attendee).' if len(self.reg_nums) > 1 else '.'}

Payments can be made in as many instalments as you wish, as long as full payment is received by 31 March 2020.

Please send proof of payment for any payments made to [vanwykk@gmail.com](mailto:vanwykk@gmail.com) and [david.shone.za@gmail.com](mailto:david.shone.za@gmail.com).

## Cancellations {{-}}

* If your registration is cancelled before 1 April 2020, 90% of the payments you have made will be refunded.
* Cancellations after 1 April will not be refunded as the full expenses will already have been incurred for the registration.

Thank you again for registering for the 2020 MD410 Convention.
"""
        )

    def __attrs_post_init__(self):
        self.names = []
        self.reg_nums = [att.reg_num for att in self.attendees]
        self.out = [f"# Attendee Details - Registered on {self.attendees[0].timestamp:%d/%m/%y at %H:%M} {{-}}"]
        for (n, att) in enumerate(self.attendees, 1):
            if n == 1:
                self.out.append(f"## First Attendee {{-}}")
            else:
                self.out.append("")
                self.out.append(
                    f"## {'Lion' if att.lion else 'Non-Lion'} Partner {{-}}"
                )
            self.out.extend(att.render())
            self.__make_names(att)
        self.names = "_".join(self.names)
        self.out.append("")
        self.out.append("## Registration Details {-}")
        self.out.append("")
        self.out.extend(self.registration.render())
        self.cost = self.registration.cost
        if self.extras:
            self.out.append("")
            self.out.append("## Extra Items {-}")
            self.out.append("")
            self.out.extend(self.extras.render())
            self.cost += self.extras.cost
        self.out.append("")
        self.out.append(f"# Total Cost: R{self.cost} {{-}}")
        self.out.append("")
        self.out.append("")
        self.__payment_details()
        self.fn = f"mdc2020_registration_{'_'.join([f'{rn:03}' for rn in self.reg_nums])}_{self.names}.txt"
        if self.out_dir:
            self.fn = os.path.join(self.out_dir, self.fn)

    def save(self):
        with open(self.fn, "w") as fh:
            fh.write("\n".join(rf.out))

class DB(common_db):

    def download_registree(self, reg_num, out_dir=None):
        tr = self.tables["registree"]
        trp = self.tables["registree_pair"]
        tc = self.tables["club"]
        tpp = self.tables["partner_program"]
        tfr = self.tables["full_reg"]
        tpr = self.tables["partial_reg"]
        tp = self.tables["pins"]

        full_reg = 0
        banquet = 0
        convention = 0
        theme = 0
        pins = 0

        try:
            second_reg_num = self.engine.execute(
                trp.select(whereclause=trp.c.first_reg_num == reg_num)
            ).fetchone()[1]
            reg_nums = [reg_num, second_reg_num]
        except Exception:
            second_reg_num = None
            reg_nums = [reg_num]

        attendees = []
        for rn in reg_nums:
            if rn:
                db_data = self.engine.execute(
                    tr.select(whereclause=tr.c.reg_num == rn)
                ).fetchone()
                data = {"reg_num": rn}
                if db_data:
                    for a in (
                        "timestamp",
                        "first_names",
                        "last_name",
                        "cell",
                        "email",
                        "dietary",
                        "disability",
                        "name_badge",
                        "first_mdc",
                        "mjf_lunch",
                        "pdg_breakfast",
                        "sharks_board",
                        "golf",
                        "sight_seeing",
                        "service_project",
                    ):
                        try:
                            d = db_data[a]
                            d = d.strip()
                        except Exception as e:
                            d = db_data[a]
                        data[a] = d
                    if db_data["is_lion"]:
                        try:
                            club = self.engine.execute(
                                tc.select(whereclause=tc.c.reg_num == rn)
                            ).fetchone()[1]
                        except Exception:
                            club = "Not Specified"
                        data["club"] = club
                        attendees.append(LionRegistree(**data))
                    else:
                        try:
                            pp = bool(
                                self.engine.execute(
                                    tpp.select(whereclause=tpp.c.reg_num == rn)
                                ).fetchone()[1]
                            )
                        except Exception:
                            pp = False
                        data["partner_program"] = pp
                        attendees.append(NonLionRegistree(**data))

                try:
                    full_reg += self.engine.execute(
                        tfr.select(whereclause=tfr.c.reg_num == rn)
                    ).fetchone()[1]
                except Exception:
                    pass
                try:
                    partial = self.engine.execute(
                        tpr.select(whereclause=tpr.c.reg_num == rn)
                    ).fetchone()
                    banquet += partial["banquet_quantity"]
                    convention += partial["convention_quantity"]
                    theme += partial["theme_quantity"]
                except Exception:
                    pass

                try:
                    pins += self.engine.execute(
                        tp.select(whereclause=tp.c.reg_num == rn)
                    ).fetchone()[1]
                except Exception:
                    pass

        registration = Registration(
            full=full_reg, banquet=banquet, convention=convention, theme=theme
        )
        extras = Extras(pins=pins)
        return RegistrationRecord(attendees, registration, extras, out_dir)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Build MD410 2020 Convention registration record"
    )
    parser.add_argument("reg_num", type=int, help="The first reg_num to use.")
    parser.add_argument(
        "--out_dir", default="/io/", help="The directory to write output to."
    )
    parser.add_argument("--fn", action="store_true", help="Output resulting filename")
    args = parser.parse_args()

    db = DB()
    rf = db.download_registree(args.reg_num, args.out_dir)
    rf.save()
    if args.fn:
        print(rf.fn)
